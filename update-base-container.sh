#!/usr/bin/bash

echo "Building base container..."

CWD=`pwd`;

docker build -t panda5-base:latest containers/base

docker save panda5-base:latest | sudo docker-squash -t jaen/panda5-base:latest-squashed -verbose | docker load

echo "Done..."
