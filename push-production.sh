#!/usr/bin/bash

echo "Pushing production container..."

docker tag -f panda5vnext_app-production:latest jaen/panda5-app:latest

docker push jaen/panda5-app:latest

echo "Done..."
