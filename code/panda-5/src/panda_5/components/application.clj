(ns panda-5.components.application
  (:require [com.stuartsierra.component :as component]
            [taoensso.timbre :as log]))

(defrecord Application [handler]
  component/Lifecycle
    (start [component]
      (let [handler (fn [req]
                      {:status  200
                       :headers {"Content-Type" "text/html"}
                       :body    "test"})]
        (assoc component :handler handler)))

    (stop [component]
      (when handler
        component)))

(defn make [& [options]]
  (map->Application {}))
