(ns panda-5.components.webserver
  (:require [com.stuartsierra.component :as component]
            [immutant.web :as immutant]
            [taoensso.timbre :as log]
            [prone.middleware :as prone]))

(defn- wrap-development [handler]
  (prone/wrap-exceptions handler))

(defrecord WebServer [environment options server application]
  component/Lifecycle
    (start [component]
      (log/info "Starting webserver with: " options)
      (let [handler (if (= environment :development)
                      (wrap-development (:handler application))
                      (:handler application))
            server  (immutant/run handler options)]
        (assoc component :server server)))

    (stop [component]
      (when server
        (log/info "Stopping webserver.")
        (immutant/stop server)
        component)))

(defn make [& [options]]
  (map->WebServer {:environment :development
                   :options (merge {:host "0.0.0.0" :port 3000}
                                    options)}))
