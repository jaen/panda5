(ns panda-5.core
  (:require [panda-5.config :as config]
            [panda-5.components.application :as application]
            [panda-5.components.webserver :as webserver]
            [environ.core :as environ]
            [taoensso.timbre :as log]
            [com.stuartsierra.component :as component])
  (:gen-class))

;; System

  (defn make-system!
    "Returns a system representing the application."
    []

    (let [{:keys [env port ssl-port keystore-path
                  truststore-path] :as config}    (config/get)
          immutant-params {:host "0.0.0.0"
                           :port port
                           :ssl-port ssl-port
                           :http2? true
                           :keystore keystore-path
                           :key-password "panda5"
                           :truststore truststore-path
                           :trust-password "panda5"}]

      (log/info "Application config: " config)
      (component/system-map
        :application (application/make)
        :web         (component/using (webserver/make immutant-params) [:application]))))

  (defn -main [& args]
    (let [system (make-system!)]
      (component/start system)))
