(ns panda-5.config
  (:require [environ.core :as environ]))

;;

  (defn- get-config-value [key default & [cast-fn]]
    (let [cast-fn (or cast-fn identity)]
      (or (some-> environ/env key cast-fn)
          default)))

;; Return configuration

  (defn integer [val]
    (Integer. val))

  (defn get []
    (let [env             (get-config-value :panda5-env            :development  keyword)
          port            (get-config-value :panda5-port           8080          integer)
          ssl-port        (get-config-value :panda5-ssl-port       8443          integer)
          db-host         (get-config-value :panda5-db-host        "localhost")
          db-port         (get-config-value :panda5-db-port        5432          integer)
          db-database     (get-config-value :panda5-db-database    "panda5_dev")
          db-user         (get-config-value :panda5-db-user        "panda5")
          db-password     (get-config-value :panda5-db-password    "panda5")
          keystore-path   (get-config-value :panda5-ssl-keystore   "certs/server.keystore")
          truststore-path (get-config-value :panda5-ssl-truststore "certs/server.keystore")
          mock-api?       (get-config-value :panda5-ssl-truststore nil (fn [value]
                                                                         (when-not (nil? value)
                                                                            (= "true" value))))
          db-params {:host db-host
                     :port db-port
                     :database db-database
                     :username db-user
                     :password db-password}]
      {:env env
       :port port
       :ssl-port ssl-port
       :db db-params
       :mock-api? mock-api?
       :keystore-path keystore-path
       :truststore-path truststore-path}))
