#!/usr/bin/env bash

java $JAVA_OPTS -server -XX:+UseCompressedOops -XX:+UseG1GC -XX:MaxGCPauseMillis=200 \
    -XX:ParallelGCThreads=4 -XX:ConcGCThreads=2 -XX:InitiatingHeapOccupancyPercent=75 \
    -jar /root/app/app.jar;
