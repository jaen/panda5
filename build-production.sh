#!/usr/bin/bash

echo "Building production container..."

CWD=`pwd`;

sudo chown $USER:$USER -R code;
sudo chown $USER:$USER -R data/maven-cache;

cd code/panda-52 &&
BOOT_LOCAL_REPO=$CWD/data/maven-cache/repository ./boot build &&
mkdir -p $CWD/containers/app-production/app/ &&
cp target/panda-5-*.jar $CWD/containers/app-production/app/app.jar;

cd $CWD && docker-compose -f docker-compose-make-production-image.yml build app-production

echo "Done..."
